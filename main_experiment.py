#!/usr/bin/python

"""

CS540 Term project experiment

* I converted this script to use medium-level API
  (no changes in actual function)
* It can be run with external Openflow controller.
  The way to run this script with external controller (on Sangwook's desktop):
  0. set 'runWithExternalController' to 'True'
  1. Run Ryu Openflow controller in same machine
     ryu-manager ~/ryu/ryu/app/simple_switch.py
  2. Run this script
     sudo python ~/cs540_netarchi/mobility.py
* Currently this script does not rely on Openflow controller,
  so this script works whatever controller runs.
  (Currently script just requires connection to the controller) 
  However, this is just an quick-and-dirty version.. There are many things to improve.

* Topology
  Take a look at Lucidchart diagram in google drive directory.
  (Not implmeneted yet)

TODOs
  Implement handover process using Openflow controller. ( )
  Design experiment topology        (v)
  Implement the topology in this file.  ( )

"""

from mininet.net import Mininet
from mininet.node import OVSSwitch
from mininet.util import quietRun
from mininet.log import output, warn
from mininet.topo import Topo
from mininet.util import dumpNodeConnections
from mininet.node import CPULimitedHost

from random import randint
from re import findall

from mininet.cli import CLI
from mininet.node import Controller, Node
from mininet.node import RemoteController
import mininet_example_nat


#from mininet.link import TCLink, Link, Intf
from mininet.link import TCLink, Intf
import mininet.ns3
from mininet.ns3 import WIFISegment, WIFIApStaLink

import ns.netanim
import time


def printConnections( switches ):
    "Compactly print connected nodes to each switch"
    for sw in switches:
        output( '%s: ' % sw )
        for intf in sw.intfList():
            link = intf.link
            if link:
                intf1, intf2 = link.intf1, link.intf2
                remote = intf1 if intf1.node != sw else intf2
                output( '%s(%s) ' % ( remote.node, sw.ports[ intf ] ) )
        output( '\n' )


def moveHost( host, oldSwitch, newSwitch):
    "Move a host from old switch to new switch"
    hintf, sintf = host.connectionsTo( oldSwitch )[ 0 ]
    oldSwitch.moveIntf( sintf, newSwitch)
    return hintf, sintf


def mobilityTest():
    "A simple test of mobility"
    #runWithExternalController = False
    runWithExternalController = True

    print '* Simple mobility test'
    if runWithExternalController is True:
        print '* Adding remote controller'
        net = Mininet( autoSetMacs=True, autoStaticArp=True,link=TCLink )
        c1 = net.addController( 'c1-ryu', controller=RemoteController, ip='127.0.0.1', port=6633)
    else:
        net = Mininet( controller=Controller, autoStaticArp=True,link=TCLink)
        c1 = net.addController( 'c1-default', port=6633)

    print '* Adding 6 hosts'
    h1 = net.addHost( 'h1', ip = '10.0.0.1' )
    h2 = net.addHost( 'h2', ip = '10.0.0.2' )
    h3 = net.addHost( 'h3', ip = '10.0.0.3' )
    h4 = net.addHost( 'h4', ip = '10.0.0.4' )

    # In our experiment, h6 and h7 is considered as one moving host.
    h5 = net.addHost( 'h5', ip = '10.0.0.5' )
    h6 = net.addHost( 'h6', ip = '10.0.0.6' )

    # h7 runs server.
    h7 = net.addHost( 'h7', ip = '10.0.0.7' )

    print '* Adding 4 switches, s1, s2 s3 and s4.'
    s1 = net.addSwitch( 's1')
    s2 = net.addSwitch( 's2')
    s3 = net.addSwitch( 's3')
    s4 = net.addSwitch( 's4')

    #linkopt1 = {'bw':1000, 'delay':'5ms'}
    linkopt1 = dict(bw=1000, delay='0.1ms')
    #linkopt1 = dict(bw=14.4, delay='10ms', max_queue_size=10)
    #linkopt2 = dict(bw=14.4, delay='5ms')
    #linkopt3 = dict(bw=1, delay='5ms', max_queue_size=1000)

    print '* Adding links between h# and s#'
    net.addLink(s4, h7, **linkopt1)

    WIFIApStaLink(s2, h1)
    WIFIApStaLink(s2, h2)
    WIFIApStaLink(s3, h3)
    WIFIApStaLink(s3, h4)

    WIFIApStaLink(s3, h5)
    WIFIApStaLink(s2, h6)

    host_list = [h1, h2, h3, h4, h5, h6, h7]
    for host in host_list:
        color = (255, 0, 0)
        mininet.ns3.setMobilityModel (host, None)
        ns.netanim.AnimationInterface.SetNodeDescription (host.nsNode, host.name)
        ns.netanim.AnimationInterface.SetNodeColor (host.nsNode, color[0], color[1], color[2])

    switch_list = [s1, s2, s3, s4]
    for switch in switch_list:
        color = (0, 0, 255)
        mininet.ns3.setMobilityModel (switch, None)
        ns.netanim.AnimationInterface.SetNodeDescription (switch.nsNode, switch.name)
        ns.netanim.AnimationInterface.SetNodeColor (switch.nsNode, color[0], color[1], color[2])

    mininet.ns3.setPosition(h7, 20.0, 20.0, 0.0)

    mininet.ns3.setPosition(h3, 50.0, 30.0, 0.0)
    mininet.ns3.setPosition(h4, 70.0, 30.0, 0.0)
    mininet.ns3.setPosition(h1, 80.0, 30.0, 0.0)
    mininet.ns3.setPosition(h2, 110.0, 30.0, 0.0)

    mininet.ns3.setPosition(h5, 10.0, 20.0, 0.0)
    mininet.ns3.setVelocity(h5, 2.0, 0.0, 0.0)
    mininet.ns3.setPosition(h6, 10.0, 20.0, 0.0)
    mininet.ns3.setVelocity(h6, 2.5, 0.0, 0.0)

    mininet.ns3.setPosition(s1, 60.0, 150.0, 0.0)
    mininet.ns3.setPosition(s4, 20.0, 100.0, 0.0)

    mininet.ns3.setPosition(s3, 50.0, 100.0, 0.0)
    mininet.ns3.setPosition(s2, 90.0, 100.0, 0.0)


    print '* Adding links between s1 and others. (tree-like)'
    net.addLink( s1, s2 , **linkopt1)
    net.addLink( s1, s3 , **linkopt1)
    net.addLink( s1, s4 , **linkopt1)

    ns.wifi.YansWifiPhyHelper().Default().EnablePcapAll("./pcap-log/wifi")
    anim = ns.netanim.AnimationInterface("./main_experiment.xml")
    anim.EnablePacketMetadata (True)

    print '* Starting network:'
    net.build()
    c1.start()
    s1.start( [ c1 ] )
    s2.start( [ c1 ] )
    s3.start( [ c1 ] )
    s4.start( [ c1 ] )


    net.start()
    mininet.ns3.start()

    #h5.cmd('sh ./experiments/ex1/h5.sh &')
    #h6.cmd('sh ./experiments/ex1/h6.sh &')


    print '* Testing network'
    printConnections( net.switches )

    # except h6: moving experiment is from (h5-s3) to (h6-s2).
    host_to_ping = [h1, h2, h3, h4, h5, h7]
    net.pingFull(hosts=host_to_ping)
    #net.pingAll()

    '''
    #print '* Testing bandwidth between h1 and h2'
    net.iperf( (h1, h2) )

    #print '* Testing bandwidth between h1 and h3'
    net.iperf( (h1, h3) )
    '''
    time.sleep(8)
    #h5.cmd('ping -i 0.1 -c 500 10.0.0.7 > ./experiments/ex1/out/h5_to_h7_ping.log &')
    #h6.cmd('ping -i 0.1 -c 500 10.0.0.7 > ./experiments/ex1/out/h6_to_h7_ping.log &')

    h5.cmd('ping -i 0.5 -D -c 500 10.0.0.7 > ./experiments/ex1/out/h5_to_h7_ping.log &')
    h6.cmd('ping -i 0.5 -D -c 500 10.0.0.7 > ./experiments/ex1/out/h6_to_h7_ping.log &')

    #h7.cmd('sh ./experiments/ex1/h7.sh > ./experiments/ex1/out/h7.log');
    #h7.cmd('sh ./experiments/ex1/out/ex1.sh');
    
    #rootNode = mininet_example_nat.connectToInternet( net )

    CLI(net)

    #mininet_example_nat.stopNAT( rootNode )
    mininet.ns3.stop()
    mininet.ns3.clear()
    net.stop()


if __name__ == '__main__':
    mobilityTest()
