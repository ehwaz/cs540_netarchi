
import logging
from ryu.controller import event

class EventMoveHost(event.EventBase):
    def __init__(self, old_host_name, new_host_name, old_switch, new_switch):
        super(EventMoveHost, self).__init__()
        self.old_host_name = old_host_name
        self.new_host_name = new_host_name
        self.old_switch = old_switch
        self.new_switch = new_switch

    def __str__(self):
        return '%s<host_name=%s, old_switch_name=%s, new_switch_name=%s>' \
                (self.__class__.__name__, 
                        self.old_host_name, self.new_host_name,
                        self.old_switch, self.new_switch)
