### CS540 넷아키 텀플젝 ###

#### 개발 환경 설정 ####
* 기본 설정
    * [Link](https://github.com/mininet/mininet/wiki/Link-modeling-using-ns-3#midterm) 에서 제공하는 Ubuntu VM을 사용. 드랍박스 다운로드 링크: [Link](https://www.dropbox.com/s/nce813ropsz16wf/midterm.zip)
    * .ova 파일을 Virtualbox에서 import해서 가져온다. 자세한 방법은 [Link](http://pcgeeks.tistory.com/7612) 참조.
    * VM 홈 디렉토리 설명
        * mininet: mininet 에뮬레이터. ns3의 모듈을 가져와 쓰기 위한 custom 구현이 mininet/mininet/ns3.py에 포함되어 있다. mininet/example/ns3에는 ns3의 모듈을 가져와 쓰는 예시 configuration 들이 있다.
        * ns-allinone-3.17: ns3 에뮬레이터.
        * of-dissector: Openflow switch에 wireshark를 붙일 수 있게 해주는 extension인듯? 아직 잘 모르겠다.
        * oflops: Openflow switch의 benchmarking suite라는데.. 아직 잘 모르겠다.
        * pox: Python 기반 SDN controller.
        * openflow: openflow 프로토콜 구현체.
* 이 repository clone 및 구현한 부분 반영하기
     * Virtualbox에 설치된 Ubuntu VM에서 인터넷이 작동하도록 virtualbox 설정을 변경한다. 방법은 [Link](http://blog.lansi.kr/archives/603)를 참조하되, 하다가 잘안되면 상욱에게 물을 것! 이거 삽질하는거 엄청 짜증나니깐 혼자 끙끙대지 말 것
        * 인터넷이 되는지 여부는 ping google.com 을 실행시 멈추지 않고 뭔가 진행되는지를 확인하면 됨.
     * Ubuntu 로그인 아이디/비번은 mininet/mininet.
     * 홈 디렉토리에서 아래 커맨드 실행하여 이 프로젝트 repo를 가져옴
        * git clone https://ehwaz@bitbucket.org/ehwaz/cs540_netarchi.git
     * 가져온 디렉토리 안에서 sh ./copy_to_mininet_dir.sh 실행하여 이 repo 안에서 수정한 사항들을 mininet이나 ns3 디렉토리에 반영.
     * ns-allinone-3.17/ns-3.17 디렉토리로 이동, 아래 커맨드 차례로 실행하여 빌드 및 환경변수 설정
        * ./waf -d optimized --enable-sudo configure
        * ./waf build
        * sudo ./waf shell
     * 이제 mininet 디렉토리에서 mininet + ns3 예제를 실행해볼수 있다.
        * 예: python mininet/examples/ns3/wifi-positions.py
    * 상욱의 데탑 접근 방법
        * 아이디@서버 / 비번 : friends@lab.masangwook.kr / kaist_cs540

## ToDo list ####
* WiFi 모듈을 mininet+ns3에서 사용하는 간단한 예제 만들기 (v)
* Handover 과정 생각하기
* Network health를 측정할 metric를 생각하기
    * Handover 과정과 health 측정은 조사한 paper를 참고해보자.
* SDN controller에 health를 분석하는 application 구현하기
* SDN controller에 handover를 중재하는 application 구현하기

#### 실험 방법 ####
* 실험 방법
    * 구현중...