set xtic auto
set ytic auto
set title "Goodputs"
set xlabel "Time (sec)"
set ylabel "Goodputs (MBytes)"

#set label "h1 starts to send h3" at 6.0,3.47
#set arrow from 6.0,3.47 to 5.0,3.66
#set key title "Goodputs on h2"

set terminal postscript enhanced eps 30 color
set output 'line.eps'

plot 'h5.dat' title 'h5' w lp, \
    'h6.dat' title 'h6' w lp

!epstopdf line.eps

