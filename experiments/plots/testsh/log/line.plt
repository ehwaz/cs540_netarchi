set xtic auto
set ytic auto
set title "Goodputs on h2 from h1"
set xlabel "Time (sec)"
set ylabel "Goodputs (GBytes)"

set label "h1 starts to send h3" at 6.0,3.47
set arrow from 6.0,3.47 to 5.0,3.66
#set key title "Goodputs on h2"

#set terminal postscript enhanced eps 30 color
#set output 'line.eps'

plot 'ret.log' title 'goodputs' w lp

#!epstopdf line.eps

