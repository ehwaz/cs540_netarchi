import ns.core
import ns.lte
import ns.mobility
import ns.network
#import ns.config_store
import ns.buildings

lteHelper = ns.lte.LteHelper()

enbNodes = ns.network.NodeContainer()
ueNodes = ns.network.NodeContainer()
enbNodes.Create(1)
ueNodes.Create(1)

mobilityHelper = ns.mobility.MobilityHelper()
mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel")
mobilityHelper.Install(enbNodes)
mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel")
mobilityHelper.Install(ueNodes)

enbDevs = ns.network.NetDeviceContainer()
ueDevs = ns.network.NetDeviceContainer()

lteHelper.Attach(ueDevs, enbDevs.Get(0))

epsBearer = ns.lte.EpsBearer(ns.lte.EpsBearer.GBR_CONV_VOICE)
lteHelper.ActivateDataRadioBearer(ueDevs, epsBearer)
lteHelper.EnableTraces()

ns.core.Simlulator.Stop(3600)
ns.core.Simlulator.Run()
ns.core.Simlulator.Destroy()

