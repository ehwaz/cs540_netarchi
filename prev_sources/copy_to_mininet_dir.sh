
# Check if mininet directory exists
if [ ! -d "../mininet" ]; then
	echo "mininet directory is not found! It must be located in the same directory as current directory!"
	exit
fi

# Overwrite ns3.py file with modified one.
cp ns3.py ../mininet/mininet/ns3.py
