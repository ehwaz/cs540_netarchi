# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
An OpenFlow 1.0 L2 learning switch implementation.

Sangwook:
    Extended sample app from ryu tutuorial.
"""

import logging
import struct
import sys

from ryu.base import app_manager
#from ryu.controller import mac_to_port
from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_0
from ryu.lib.mac import haddr_to_bin
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ipv4

import ryu.topology.api as topology_api
import ryu.topology.switches as topology_class
from ryu.lib import hub
import dijkstra
import ryu_events
from ryu.ofproto import ether
import ryu.lib.ip as ip_addr

import time
import datetime

topology_1 = {
        'h1': { 's2':1 },
        'h2': { 's2':1 },
        'h3': { 's3':1 },
        'h4': { 's3':1 },
        'h5': { 's3':1 },
        'h6': { 's2':1 },
        'h7': { 's4':1 },
        's1': { 's2':1, 's3':1, 's4':1 },
        's2': { 's1':1, 'h1':1, 'h2':1 },
        's3': { 's1':1, 'h3':1, 'h4':1 },
        's4': { 's1':1, 'h7':1 }
        }
        
loss_rate_threshold = 0.1

class Link_ex(topology_class.Link):
    def __init__(self, src, dst):
        self.src = src
        self.dst = dst

    def __eq__(self, other):
        return self.src == other.src and self.dst == other.dst

    def __str__(self):
        result = "src: %s, dst: %s" % (self.src, self.dst)
        return result

class Switch():
    def __init__(self, name=None, dpid=None, datapath_obj=None):
        self.name = name
        self.ports = {}
        self.dpid = dpid
        self.num_host = 0
        self.datapath_obj = datapath_obj
        self.host_port_dict = {}

    def addPort(self, new_port_no, new_port_addr):
        self.ports[new_port_no] = new_port_addr 

class Host():
    def __init__(self, dst_mac_addr, ip_addr=None, dst_port=None, dst_dpid=None, dst_dp_name=None, name=None):
        self.dst_mac_addr   = dst_mac_addr
        self.ip_addr    = ip_addr
        self.dst_port   = dst_port
        self.dst_dpid   = dst_dpid
        self.my_mac_addr    = None
        self.switch_name = dst_dp_name
        self.name = name

    def setIpAddr(self, ip_addr):
        self.ip_addr = ip_addr

    def setDstPort(self, dst_port):
        self.dst_port = dst_port

    def setDstSwitch(self, dst_dpid):
        self.dst_dpid = dst_dpid

    def setMyMacAddr(self, mac_addr):
        self.my_mac_addr = mac_addr

    def getMyMacAddr(self):
        return self.my_mac_addr

    def setName(self, name):
        self.name = name

    def __str__(self):
        result = "dst mac addr: %s" % self.dst_mac_addr
        if self.dst_dpid is not None:
            result += ", dst_dpid: %s" % self.dst_dpid
        if self.my_mac_addr is not None:
            result += ", my_mac_addr: %s" % self.my_mac_addr
        if self.dst_port is not None:
            result += ", dst_port: %s" % self.dst_port
        if self.name is not None:
            result += ", name: %s" % self.name
        return result

class PathSegment():
    def __init__(self, in_port, switch, out_port):
        self.in_port = in_port
        self.switch = switch
        self.out_port = out_port

class SimpleSwitch(app_manager.RyuApp):
    # host(=src ip addr) : Id of the switch which handles that src id addr
    OFP_VERSIONS = [ofproto_v1_0.OFP_VERSION]
    _EVENTS = [ryu_events.EventMoveHost]

    def __init__(self, *args, **kwargs):
        super(SimpleSwitch, self).__init__(*args, **kwargs)

        self.sw_list = []
        self.link_list = []
        self.host_dict = {}
        self.temp_host_list = []

        self.mac_to_port = {}

        self._topology_check_thread = None
        self.switches = {}
        self.switch_name_to_dpid = {}
        self.switch_dpid_to_name = {}
        self.host_mac_to_name_ip = {}

        self._temp_thread_to_trigger = hub.spawn(self._temp_func_for_trigger_thread)
        self.disableLearningSwitch = False
        
        #self._monitor_thread = hub.spawn(self._monitor)
        self._monitor2_thread = None
        self.traffic_info = {}
        # self.traffic_info {dpid:{'bytes':0, 'traffic':0, 'old_byte':0, 'average_traffic':0}, ...}
        self.packet_loss_info = {}
        # self.packet_loss_info {dpid:{port:{'old_packets':0, 'packet_loss_rate':0}, ...}, ...}
        
    """
    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, match, actions, 0)
    """

    def add_flow(self, datapath, dst_addr, actions, in_port=None):
        ofproto = datapath.ofproto
        if in_port is not None:
            match = datapath.ofproto_parser.OFPMatch(
                in_port=in_port, dl_dst=haddr_to_bin(dst_addr))
        else:
            match = datapath.ofproto_parser.OFPMatch(
                dl_dst=haddr_to_bin(dst_addr))

        mod = datapath.ofproto_parser.OFPFlowMod(
            datapath=datapath, match=match, cookie=0,
            command=ofproto.OFPFC_ADD, idle_timeout=0, hard_timeout=0,
            priority=ofproto.OFP_DEFAULT_PRIORITY,
            flags=ofproto.OFPFF_SEND_FLOW_REM, actions=actions)
        datapath.send_msg(mod)

    def add_flow_fake_ip(self, datapath, dst_addr, actions, in_port=None):
        # Make fake ip address from mac address.
        host_num = dst_addr[len(dst_addr)-1]
        fake_ip_addr = "10.0.0." + str(host_num)

        #ip_dst = ip_addr.ipv4_to_bin(fake_ip_addr)
        ip_dst = struct.unpack('!I', ip_addr.ipv4_to_bin(fake_ip_addr))[0]

        print "add_flow_fake_ip: fake ip %s from %s" % (fake_ip_addr, dst_addr)

        ofproto = datapath.ofproto
        if in_port is not None:
            match = datapath.ofproto_parser.OFPMatch(
                in_port=in_port, dl_type=ether.ETH_TYPE_IP, nw_dst=ip_dst)
        else:
            match = datapath.ofproto_parser.OFPMatch(
                dl_type=ether.ETH_TYPE_IP, nw_dst=ip_dst)

        mod = datapath.ofproto_parser.OFPFlowMod(
            datapath=datapath, match=match, cookie=0,
            command=ofproto.OFPFC_ADD, idle_timeout=0, hard_timeout=0,
            priority=ofproto.OFP_DEFAULT_PRIORITY,
            flags=ofproto.OFPFF_SEND_FLOW_REM, actions=actions)
        datapath.send_msg(mod)

    def add_flow_2(self, datapath, match, actions):
        ofproto = datapath.ofproto
        mod = datapath.ofproto_parser.OFPFlowMod(
            datapath=datapath, match=match, cookie=0,
            command=ofproto.OFPFC_ADD, idle_timeout=0, hard_timeout=0,
            priority=ofproto.OFP_DEFAULT_PRIORITY,
            flags=ofproto.OFPFF_SEND_FLOW_REM, actions=actions)
        datapath.send_msg(mod)

    def remove_flow(self, datapath, match, actions):
        ofproto = datapath.ofproto
        flow_mod = datapath.ofproto_parser.OFPFlowMod(
            datapath=datapath, match=match, cookie=0,
            command=ofproto.OFPFC_DELETE, idle_timeout=0, hard_timeout=0,
            priority=ofproto.OFP_DEFAULT_PRIORITY,
            flags=ofproto.OFPFF_SEND_FLOW_REM, actions=actions)
        datapath.send_msg(flow_mod)

    def findPortByPathSegment(self, seg_head_name, seg_tail_name):
        seg_head_dpid = self.switch_name_to_dpid[seg_head_name]
        seg_tail_dpid = self.switch_name_to_dpid[seg_tail_name]
        for link in self.link_list:
            if link.src.dpid == seg_head_dpid and link.dst.dpid == seg_tail_dpid:
                return link.src.port_no, link.dst.port_no
        print "Error: there must exist a matching link for the given path segment!"
        sys.exit(0)

    def move_host(self, old_host_name, new_host_name, old_switch_name, new_switch_name):
        old_host_obj = self.host_dict[old_host_name]
        new_host_obj = self.host_dict[new_host_name]
        assert old_host_obj.my_mac_addr is not None
        assert new_host_obj.my_mac_addr is not None

        # 1. get shortest path between old_swtch and new_switch
        path = dijkstra.shortestPath(topology_1, old_switch_name, new_switch_name)
        print path

        # 2. get out_port number and in_port number in each path segment
        pathSegmentList = []
        head_switch = None
        in_port = None
        out_port = None
        for tail_switch in path:
            if head_switch is None:
                head_switch = tail_switch
            else:
                head_out_port, tail_in_port = self.findPortByPathSegment(head_switch, tail_switch)

                out_port = head_out_port
                if in_port is None:
                    # First segment for old_switch: in_port is connected to the host
                    in_port = self.switches[old_switch_name].host_port_dict[old_host_name]
                # each segment: in_port -> switch -> out_port
                print "Path segment: in_port:%s, switch:%s, out_port:%s" % (in_port, head_switch, out_port)
                new_segment = PathSegment(in_port, head_switch, out_port)
                pathSegmentList.append(new_segment)
                # for the next PathSegment
                head_switch = tail_switch
                in_port = tail_in_port

        # Last segment: out_port is connected to the host
        out_port = self.switches[new_switch_name].host_port_dict[new_host_name]
        new_segment = PathSegment(in_port, head_switch, out_port)
        print "Path segment: in_port:%s, switch:%s, out_port:%s" % (in_port, head_switch, out_port)
        pathSegmentList.append(new_segment)

        # TODO: change it to input host's ip_addr
        old_host_mac_addr = old_host_obj.my_mac_addr
        new_host_mac_addr = new_host_obj.my_mac_addr
        for segment in pathSegmentList:
            # TODO: Barrier to sync two Openflow message?
            # for each segment in the path,

            # 3. add new flow rule 'if to A, to in_port' to new_datapath with lower priority than current rule.
            new_switch_obj = self.switches[segment.switch]
            new_switch_dp_obj = new_switch_obj.datapath_obj
            ofp_parser = new_switch_dp_obj.ofproto_parser

            # Match: to host's ip addr. Action: send packet to the some port.
            #match = ofp_parser.OFPMatch(dl_dst=haddr_to_bin(new_host_mac_addr))

            '''
            match = ofp_parser.OFPMatch(dl_dst=haddr_to_bin(new_host_mac_addr))
            actions = [ofp_parser.OFPActionOutput(segment.out_port)]
            self.add_flow_2(new_switch_dp_obj, match, actions)
            '''
            actions = [ofp_parser.OFPActionOutput(segment.out_port)]

            #self.add_flow_fake_ip(new_switch_dp_obj, new_host_mac_addr, actions)
            self.add_flow(new_switch_dp_obj, new_host_mac_addr, actions)

            # 4. remove flow rule 'if to A, to out_port' from old_datapath
            # As this enables added flows in step 3, actually it works same as "modify" command.
            old_switch_obj = self.switches[old_switch_name]
            old_switch_dp_obj = old_switch_obj.datapath_obj
            ofp_parser = old_switch_dp_obj.ofproto_parser

            # Match: to host's ip addr. Action: no action.
            #match = ofp_parser.OFPMatch(dl_dst=haddr_to_bin(old_host_mac_addr))
            match = ofp_parser.OFPMatch(dl_dst=haddr_to_bin(old_host_mac_addr))
            actions = []
            self.remove_flow(old_switch_dp_obj, match, actions)

    def learningSwitchCore(self, ev):
        # ev.msg is an object that represents a packet_in data structure.
        msg = ev.msg
        # msg.datapath is an object that represents a datapath (switch).
        datapath = msg.datapath
        # dp.ofproto and dp.ofproto_parser are objects
        # that represent the OpenFlow protocol that Ryu and the switch negotiated.
        ofproto = datapath.ofproto
        ofp_parser = datapath.ofproto_parser

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocol(ethernet.ethernet)

        dst = eth.dst
        src = eth.src

        # dpid = switch id
        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})

        if eth.ethertype == ether.ETH_TYPE_IP:
            ipv4_pkt = pkt.get_protocol(ipv4.ipv4)
            ipv4_src = ipv4_pkt.src
            ipv4_dst = ipv4_pkt.dst
            self.logger.info("packet in %s src:%s dst:%s port:%s", dpid, ipv4_src, ipv4_dst, msg.in_port)

        # learn a mac address to avoid FLOOD next time.
        self.mac_to_port[dpid][src] = msg.in_port


        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD

        # OFPActionOutput class is used to specify a switch port that you want to send the packet out of.
        actions = [ofp_parser.OFPActionOutput(out_port)]

        # install a flow to avoid packet_in next time
        #if out_port != ofproto.OFPP_FLOOD:
        if out_port != ofproto.OFPP_FLOOD and self.disableLearningSwitch is False:
            #self.add_flow_fake_ip(datapath, dst, actions, msg.in_port)
            #self.add_flow_fake_ip(datapath, dst, actions)
            #self.add_flow(datapath, dst, actions, msg.in_port)

            #self.add_flow_fake_ip(datapath, dst, actions)
            self.add_flow(datapath, dst, actions)

            # TODO: this is just an fake rule setup :-(..
            host_name = 'h6'
            switch_dpid = 4 # = 's4'
            host_same_as = 'h5'
            mac_addr_host_same_as = '00:00:00:00:00:05'
            mac_addr_fake = '00:00:00:00:00:06'

            if dst == mac_addr_host_same_as and dpid == switch_dpid:
                #switch_to_install = self.switches[switch_name]
                #datapath_to_install = switch_to_install.datapath_obj
                #ofp_parser = switch_to_install.datapath_obj.ofproto_parser

                datapath_to_install = datapath
                port_to_install = out_port

                actions_t = [ofp_parser.OFPActionOutput(port_to_install)]
                #self.add_flow_fake_ip(datapath_to_install, mac_addr_fake, actions_t)
                self.add_flow(datapath_to_install, mac_addr_fake, actions_t)

        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        out = ofp_parser.OFPPacketOut(
            datapath=datapath, buffer_id=msg.buffer_id, in_port=msg.in_port,
            actions=actions, data=data)

        # Ryu builds and send the on-wire data format to the switch.
        datapath.send_msg(out)

    def prepareExperiment(self):
        # For migration experiment,
        # remove flow rules about new switch from all possible pathes.
        edd

    # Using MAIN_DISPATCHER as the second argument means
    # this function is called only after the negotiation completes.
    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        if self._topology_check_thread is None:
            self._topology_check_thread = hub.spawn(self._checkTopology)

        #if self.disableLearningSwitch is False:
            #self.learningSwitchCore(ev)
        self.learningSwitchCore(ev)

        msg = ev.msg
        in_port = msg.in_port
        datapath = msg.datapath
        ofproto = datapath.ofproto
        ofp_parser = datapath.ofproto_parser

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocol(ethernet.ethernet)
        eth_src = eth.src
        eth_dst = eth.dst

        if eth.ethertype == ether.ETH_TYPE_IP:
            ipv4_pkt = pkt.get_protocol(ipv4.ipv4)
            ipv4_src = ipv4_pkt.src
            ipv4_dst = ipv4_pkt.dst
            """
            temp_addr_list = ipv4_src.split(".")
            temp_addr_list.reverse()
            host_num = temp_addr_list[0]
            host_name = "h" + str(host_num)
            self.host_mac_to_name_ip[eth_src] = (host_name, ipv4_src)
            print "host %s learned ip addr! ip_addr:%s" % (host_name, ipv4_src)
            #print "IPv4 addr: src:%s, dst:%s" % (ipv4_src, ipv4_dst)
            print self.host_mac_to_name_ip
            """

        if self.host_mac_to_name_ip.get(eth_src) is None:
            # This code of host name resolving must be used with autoSetMacs option in Mininet()
            for host_obj in self.temp_host_list:
                #print "dst:%s, host_dst:%s" % (dst, host_obj.dst_mac_addr)
                if in_port == host_obj.dst_port and datapath.id == host_obj.dst_dpid:
                    temp_str = eth_src.replace(":", "")
                    host_num = temp_str[len(temp_str)-1]
                    host_name = "h" + str(host_num)
                    host_obj.setName(host_name)
                    #self.host_mac_to_name_ip[eth_src] = (host_name, ipv4_src)
                    self.host_mac_to_name_ip[eth_src] = (host_name, None)
                    host_obj.setMyMacAddr(eth_src)
                    self.host_dict[host_name] = host_obj
                    cur_switch = self.switches[host_obj.switch_name]
                    cur_switch.host_port_dict[host_name] = host_obj.dst_port
                    print "host %s learned mac addr! mac_addr:%s" % (host_name, eth_src)

        # TODO: remove this hard-coded value: number of host == 7
        if len(self.temp_host_list) == 7 and  len(self.host_dict.keys()) == 7 and self.disableLearningSwitch is False:
            self.disableLearningSwitch = True
            print "Experiment preparation is done!!!"
            #if self._monitor2_thread is None:
            #   hub.spawn(self._monitor2)

    def buildSwitchTopo(self):
        print "not yet"

    def _temp_func_for_trigger_thread(self):
        hub.sleep(30)
        print "@@@@@@@@@@@@@ Sending Event!!, time: %s" % str(datetime.datetime.now())
        nameOfThisApp = self.__class__.__name__
        self.send_event(nameOfThisApp, ryu_events.EventMoveHost("h5", "h6", "s3", "s2"))

    @set_ev_cls(ryu_events.EventMoveHost)
    def _host_moving_handler(self, req):
        print "@@@@@@@@@@@@@ Event Caught!!!!!"
        self.move_host(req.old_host_name, req.new_host_name,
                         req.old_switch, req.new_switch)

    def _checkTopologyCore(self):
        self.sw_list = topology_api.get_all_switch(self)

        self.link_list = []
        base_link_list = topology_api.get_all_link(self)
        for base_link in base_link_list:
            new_link = Link_ex(base_link.src, base_link.dst)
            self.link_list.append(Link_ex(base_link.src, base_link.dst))
            print "Link: " + str(new_link)

        # make uplink and downlink to one.
        trc_link_mac_addr_list = []
        trc_link_list = []
        for link_1 in self.link_list:
            for link_2 in self.link_list:
                if link_1.src == link_2.dst:
                    trc_link_list.append(link_1)
                    trc_link_mac_addr_list.append(link_1.src.hw_addr)
                    trc_link_mac_addr_list.append(link_1.dst.hw_addr)

        #self.temp_host_list = []
        for switch in self.sw_list:
            new_switch = Switch(dpid = switch.dp.id, datapath_obj=switch.dp)
            for port in switch.ports:
                switch_name = port.name.split('-')[0]
                new_switch.name = switch_name
                self.switch_name_to_dpid[switch_name] = new_switch.dpid
                self.switch_dpid_to_name[new_switch.dpid] = switch_name
                new_switch.addPort(port.port_no, port.hw_addr)
                if port.hw_addr not in trc_link_mac_addr_list:
                    new_host = Host(dst_mac_addr=port.hw_addr,
                            dst_port=port.port_no, dst_dpid=port.dpid,
                            dst_dp_name=switch_name, name="unknown")
                    new_host.setDstSwitch(switch.dp.id)
                    self.temp_host_list.append(new_host)
                    print "New host:%s" % str(new_host)
            self.switches[new_switch.name] = new_switch

        for switch_name, switch_obj in self.switches.iteritems():
            print ("Switch name: %s, dpid: %s" % (switch_name, switch_obj.dpid))
            for port_no, port_mac_addr in switch_obj.ports.iteritems():
                print ("Port number: %d, mac_addr: %s" % (port_no, port_mac_addr) )


    def _checkTopology(self):
        hub.sleep(3)
        loop = False
        self._checkTopologyCore()
        if loop is True:
            isHostDiscoveryDone = False
            while (True):
                #self._checkTopologyCore()
                # learn host's src mac_addr.
                #for host_name, host_obj in self.host_dict.iteritems():
                for host_obj in self.temp_host_list:
                    eth_src = host_obj.getMyMacAddr()
                    if eth_src is not None:
                        ip_info_pair = self.host_mac_to_name_ip.get(eth_src)
                        if ip_info_pair is not None:
                            host_name = ip_info_pair[0]
                            # TODO: not using ip address currently.
                            #host_ip = ip_info_pair[1]
                            host_obj.setName(host_name)
                            self.host_dict[host_name] = host_obj
                            cur_switch = self.switches[host_obj.switch_name]
                            cur_switch.host_port_dict[host_name] = host_obj.dst_port
                            print "Host got name: " + str(host_obj)

                if len(self.temp_host_list) == len(self.host_dict.keys()):
                    isHostDiscoveryDone = True
                else:
                    hub.sleep(5)


    @set_ev_cls(ofp_event.EventOFPPortStatus, MAIN_DISPATCHER)
    def _port_status_handler(self, ev):
        msg = ev.msg
        reason = msg.reason
        port_no = msg.desc.port_no

        ofproto = msg.datapath.ofproto
        if reason == ofproto.OFPPR_ADD:
            self.logger.info("port added %s", port_no)
        elif reason == ofproto.OFPPR_DELETE:
            self.logger.info("port deleted %s", port_no)
        elif reason == ofproto.OFPPR_MODIFY:
            self.logger.info("port modified %s", port_no)
        else:
            self.logger.info("Illegal port state %s %s", port_no, reason)

    def _monitor(self):
        count = 0
        while True:
            for switch in self.switches.values():
                if switch.dpid not in self.traffic_info:
                    self.traffic_info[switch.dpid] = {'bytes':0, 'traffic':0, 'old_bytes':0, 'average_traffic':0}
                self._request_stats(switch.datapath_obj)
            #hub.sleep(1)
            hub.sleep(3)
            
            """
            count += 1
            if (count%300) == 0:
                count = 0
                for switch in self.switches.values():
                    temp = self.traffic_info[switch.dp.id]['bytes'] - self.traffic_info[switch.dp.id]['old_bytes']
                    self.traffic_info[switch.dp.id]['old_bytes'] = self.traffic_info[switch.dp.id]['bytes']
                    self.traffic_info[switch.dp.id]['average_traffic'] = temp/300
                    #self.logger.info('datapath %016x average_traffic %8d', switch.dp.id, self.traffic_info[id]['average_traffic'])
            """
    """
    @set_ev_cls(ofp_event.EventOFPStateChange,
                [MAIN_DISPATCHER, DEAD_DISPATCHER])
    def _state_change_handler(self, ev):
        datapath = ev.datapath
        if ev.state == MAIN_DISPATCHER:
            if not datapath.id in self.datapaths:
                self.logger.debug('register datapath: %016x', datapath.id)
                self.datapaths[datapath.id] = datapath
        elif ev.state == DEAD_DISPATCHER:
            if datapath.id in self.datapaths:
                self.logger.debug('unregister datapath: %016x', datapath.id)
                del self.datapaths[datapath.id]
    """
    
    def _request_stats(self, datapath):
        #self.logger.debug('send stats request: %016x', datapath.id)
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        
        req = parser.OFPFlowStatsRequest(datapath, 0, parser.OFPMatch(), 255, ofproto.OFPP_NONE)
        datapath.send_msg(req)
        
        #req = parser.OFPPortStatsRequest(datapath, 0, ofproto.OFPP_NONE)
        #datapath.send_msg(req)
        
    @set_ev_cls(ofp_event.EventOFPFlowStatsReply, MAIN_DISPATCHER)
    def _flow_stats_reply_handler(self, ev):
        body = ev.msg.body
        new_bytes = {}
        for id in self.traffic_info:
            new_bytes[id] = self.traffic_info[id]['bytes']
        for stat in body:
            dpid = ev.msg.datapath.id
            temp = new_bytes[dpid]
            new_bytes[dpid] = temp + stat.byte_count
            #self.logger.info('in stat %016x %8d', dpid, stat.byte_count)
        
        for id in new_bytes:
            if id in self.traffic_info:
                old_byte = self.traffic_info[id]['bytes']
                self.traffic_info[id]['traffic'] = new_bytes[id] - self.traffic_info[id]['bytes']
                self.traffic_info[id]['bytes'] = new_bytes[id]
            else:
                self.traffic_info[id]['bytes'] = new_bytes[id]
                self.traffic_info[id]['traffic'] = new_bytes[id]
            self.logger.info('datapath %016x bytes %8d traffics %8d', id, self.traffic_info[id]['bytes'], self.traffic_info[id]['traffic'])
            
    def compare_network_conditions(self, switch1, switch2):
        if traffic_info[switch_name_to_dpid[switch1]]['average_traffic'] < traffic_info[switch_name_to_dpid[switch2]]['average_traffic']:
            return switch1
        else:
            return switch2
            
    def _monitor2(self):
        self.monitor2([self.host_dict["h5"], self.host_dict["h6"]], self.host_dict["h5"])
            
    def monitor2(self, hosts, main_host):
        temps = hosts
        host_to_move = None
        for host in hosts:
            if host.dst_dpid not in self.packet_loss_info:
                self.packet_loss_info[host.dst_dpid] = {host.dst_port:{'old_packets':0, 'old_dropped':0, 'packet_loss_rate':0}}
            elif host.dst_port not in self.packet_loss_info[host.dst_dpid]:
                self.packet_loss_info[host.dst_dpid][host.dst_port] = {'old_packets':0, 'old_errors':0, 'packet_loss_rate':0}
                
        while True:
            for host in hosts:
                #if not host.name == main_host.name:
                #   continue
                dp = self.switches[self.switch_dpid_to_name[host.dst_dpid]].datapath_obj
                parser = dp.ofproto_parser
                
                req = parser.OFPPortStatsRequest(dp, 0, host.dst_port)
                dp.send_msg(req)
                #hub.sleep(1)
                hub.sleep(3)
                #self.logger.info('datapath.send_msg')
                if self.packet_loss_info[host.dst_dpid][host.dst_port]['packet_loss_rate'] > loss_rate_threshold:

                    self.logger.info('monitor2 1')
                    for temp in temps:
                        self.logger.info('monitor2 2')
                        if temp.name == main_host.name:
                            continue
                        if host_to_move == None:
                            host_to_move = temp
                            continue
                        if self.traffic_info[temp.dst_dpid]['traffic'] < self.traffic_info[host_to_move.dst_dpid]['traffic']:
                            host_to_move = temp
                    nameOfThisApp = self.__class__.__name__
                    move_host(main_host.name, host_to_move.name, self.switch_dpid_to_name[main_host.dst_dpid], self.switch_dpid_to_name[host_to_move.dst_dpid])
                        
                else:
                    self.packet_loss_info[host.dst_dpid][host.dst_port]['packet_loss_rate'] = 0
            
        
    @set_ev_cls(ofp_event.EventOFPPortStatsReply, MAIN_DISPATCHER)
    def _port_stats_reply_handler(self, ev):
        body = ev.msg.body
        """
        self.logger.info('datapath         port     '
                         'rx-pkts  rx-bytes rx-error '
                         'tx-pkts  tx-bytes tx-error')
        self.logger.info('---------------- -------- '
                         '-------- -------- -------- '
                         '-------- -------- --------')
        for stat in sorted(body, key=attrgetter('port_no')):
            self.logger.info('%016x %8x %8d %8d %8d %8d %8d %8d',
                             ev.msg.datapath.id, stat.port_no,
                             stat.rx_packets, stat.rx_bytes, stat.rx_errors,
                             stat.tx_packets, stat.tx_bytes, stat.tx_errors)
        """
        #self.logger.info('port_stats_reply_handler 1')
        for stat in body:
            dpid = ev.msg.datapath.id
            if dpid in self.packet_loss_info:
                #self.logger.info('port_stats_reply_handler 2')
                if stat.port_no in self.packet_loss_info[dpid]:
                    #self.logger.info('port_stats_reply_handler 3')
                    temp_dropped = self.packet_loss_info[dpid][stat.port_no]['old_dropped'] - stat.rx_dropped
                    temp_packets = self.packet_loss_info[dpid][stat.port_no]['old_packets'] - stat.rx_packets
                    temp_loss_rate = temp_dropped / (temp_dropped + temp_packets + 0.001)
                    self.packet_loss_info[dpid][stat.port_no]['old_dropped'] = stat.rx_dropped
                    self.packet_loss_info[dpid][stat.port_no]['old_packets'] = stat.rx_packets
                    self.packet_loss_info[dpid][stat.port_no]['packet_loss_rate'] = temp_loss_rate

                    self.logger.info('in _port_stats_reply_handler dpid[%016x] port[%8d] received[%8d] dropped[%8d] error[%8d] loss_rate[%8f]', dpid, stat.port_no, stat.rx_packets, stat.rx_dropped, stat.rx_errors, temp_loss_rate)
        
