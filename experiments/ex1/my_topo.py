from mininet.topo import Topo
from mininet.log import lg
from mininet.cli import CLI
from mininet.util import dumpNodeConnections
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet

from mininet.util import pmonitor
from time import time
from signal import SIGINT
import time

class Ex1(Topo):
    def __init__(self):
        Topo.__init__(self)

        S = [0]*5
        H = [0]*9

        for s in range(4):
            S[s+1] = self.addSwitch('s%s' % (s + 1))
        for h in range(8):
            H[h+1] = self.addHost('h%s' % (h + 1))

        linkopt1 = dict(bw=1000, delay='5ms')
        linkopt2 = dict(bw=14.4, delay='5ms')
        linkopt3 = dict(bw=0.1, delay='5ms', max_queue_size=1000)

        self.addLink(S[2], H[1], **linkopt2)
        self.addLink(S[2], H[2], **linkopt2)
        self.addLink(S[2], H[5], **linkopt2)
        self.addLink(S[3], H[3], **linkopt2)
        self.addLink(S[3], H[4], **linkopt2)
        self.addLink(S[3], H[6], **linkopt3)
        self.addLink(S[4], H[7], **linkopt1)
        self.addLink(S[4], H[8], **linkopt1)

        self.addLink(S[1], S[2], **linkopt1)
        self.addLink(S[1], S[3], **linkopt1)
        self.addLink(S[1], S[4], **linkopt1)
        

def test():
    topo = Ex1()
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    net.start()
    
    h5, h6, h7 = net.get('h5', 'h6', 'h7');

    #h5p = h5.popen('sh','/home/friends/cs540_netarchi/experiments/ex1/h5.sh')
    #h6p = h6.popen('sh','/home/friends/cs540_netarchi/experiments/ex1/h6.sh')
#h5.sendCmd('sh', '/home/friends/cs540_netarchi/experiments/ex1/h5.sh');
    #h6.sendCmd('sh', '/home/friends/cs540_netarchi/experiments/ex1/h6.sh');
    #time.sleep(2)
    #h7p = h7.popen('sh','/home/friends/cs540_netarchi/experiments/ex1/h7.sh')
    #h7.sendCmd('sh', '/home/friends/cs540_netarchi/experiments/ex1/h7.sh');

    #time.sleep(11);
    #h5p.send_signal(SIGINT)
    #h6p.send_signal(SIGINT)
    #h7p.send_signal(SIGINT)
    h5.cmd('sh h5.sh &')
    h6.cmd('sh h6.sh &')
    time.sleep(2)
    h7.cmd('sh h7.sh > ./out/h7.log')
    #h7.sendCmd('sh', 'h7.sh')

    CLI(net)

    net.stop()

if __name__=='__main__':
    lg.setLogLevel('info')
    test()



    


