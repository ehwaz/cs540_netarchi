from operator import attrgetter

# import new implemented switch app by sangwook instead of simple_switch by default
# import ryu_app_mobile_switch
from ryu.app import simple_switch
from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.lib import hub
#import ryu_events

class SimpleMonitor(simple_switch.SimpleSwitch):
    def __init__(self, *args, **kwargs):
        super(SimpleMonitor, self).__init__(*args, **kwargs)
        self.datapaths = {}
        self.monitor_thread = hub.spawn(self._monitor)
        self.traffic_info = {}
        
    @set_ev_cls(ofp_event.EventOFPStateChange,
                [MAIN_DISPATCHER, DEAD_DISPATCHER])
    def _state_change_handler(self, ev):
        datapath = ev.datapath
        if ev.state == MAIN_DISPATCHER:
            if not datapath.id in self.datapaths:
                self.logger.debug('register datapath: %016x', datapath.id)
                self.datapaths[datapath.id] = datapath
        elif ev.state == DEAD_DISPATCHER:
            if datapath.id in self.datapaths:
                self.logger.debug('unregister datapath: %016x', datapath.id)
                del self.datapaths[datapath.id]

    def calculate_traffics(self):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        for switch in self.sw_list:
            self._request_stats(ev.datapath)
            req = parser.OFPFlowStatsRequest(datapath, 0, parser.OFPMatch(), 255, ofproto.OFPP_NONE)
            
                
    def _monitor(self):
        count = 0
        while True:
            for dp in self.datapaths.values():
                if dp.id not in self.traffic_info:
                    self.traffic_info[dp.id] = {'bytes':0, 'traffic':0, 'old_bytes':0, 'average_traffic':0}
                self._request_stats(dp)
            hub.sleep(1)
            count += 1
            if (count%300) == 0:
                count = 0
                for dp in self.datapaths.values():
                    temp = self.traffic_info[dp.id]['bytes'] - self.traffic_info[dp.id]['old_bytes']
                    del self.traffic_info[dp.id]['old_bytes']
                    self.traffic_info[dp.id]['old_bytes'] = self.traffic_info[dp.id]['bytes']
                    del self.traffic_info[dp.id]['average_traffic']
                    self.traffic_info[dp.id]['average_traffic'] = temp/300
                    self.logger.info('datapath %016x average_traffic %8d', dp.id, self.traffic_info[dp.id]['average_traffic'])
            
    def _request_stats(self, datapath):
        self.logger.debug('send stats request: %016x', datapath.id)
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        
        req = parser.OFPFlowStatsRequest(datapath, 0, parser.OFPMatch(), 255, ofproto.OFPP_NONE)
        datapath.send_msg(req)
        
        #req = parser.OFPPortStatsRequest(datapath, 0, ofproto.OFPP_NONE)
        #datapath.send_msg(req)
        
    @set_ev_cls(ofp_event.EventOFPFlowStatsReply, MAIN_DISPATCHER)
    def _flow_stats_reply_handler(self, ev):
        body = ev.msg.body
        new_bytes = {}
        for id in self.traffic_info:
            new_bytes[id] = self.traffic_info[id]['bytes']
        for stat in body:
            dpid = ev.msg.datapath.id
            temp = 0
            if dpid in new_bytes:
                del new_bytes[dpid]
            temp += stat.byte_count
            new_bytes[dpid] = temp 
            self.logger.info('in stat %016x %8d', dpid, stat.byte_count)
        
        for id in new_bytes:
            if id in self.traffic_info:
                old_byte = self.traffic_info[id]['bytes']
                del self.traffic_info[id]['traffic']
                self.traffic_info[id]['traffic'] = new_bytes[id] - self.traffic_info[id]['bytes']
                del self.traffic_info[id]['bytes']
                self.traffic_info[id]['bytes'] = new_bytes[id]
            else:
                self.traffic_info[id]['bytes'] = new_bytes[id]
                self.traffic_info[id]['traffic'] = new_bytes[id]
            self.logger.info('datapath %016x bytes %8d traffics %8d', id, self.traffic_info[id]['bytes'], self.traffic_info[id]['traffic'])
                
            
        
        """
        self.logger.info('datapath         '
                         'in-port   dl-dst           '
                         'out-port  packets bytes')
        self.logger.info('---------------- '
                         '-------- ----------------- '
                         '-------- -------- --------')
        for stat in body:
            self.logger.info('%016x %8x %17s %8x %8d %8d',
                             ev.msg.datapath.id,
                             stat.match.in_port, stat.match.dl_dst,
                             stat.actions[0].port,
                             stat.packet_count, stat.byte_count)
        """
        """
        for stat in sorted([flow for flow in body],
                           key=lambda flow: (flow.match['in_port'],
                                             flow.match['dl_dst'])):
            self.logger.info('%016x %8x %17s %8x %8d %8d',
                             ev.msg.datapath.id,
                             stat.match['in_port'], stat.match['dl_dst'],
                             stat.instructions[0].actions[0].port,
                             stat.packet_count, stat.byte_count)
        """                     
    @set_ev_cls(ofp_event.EventOFPPortStatsReply, MAIN_DISPATCHER)
    def _port_stats_reply_handler(self, ev):
        body = ev.msg.body
        
        self.logger.info('datapath         port     '
                         'rx-pkts  rx-bytes rx-error '
                         'tx-pkts  tx-bytes tx-error')
        self.logger.info('---------------- -------- '
                         '-------- -------- -------- '
                         '-------- -------- --------')
        for stat in sorted(body, key=attrgetter('port_no')):
            self.logger.info('%016x %8x %8d %8d %8d %8d %8d %8d',
                             ev.msg.datapath.id, stat.port_no,
                             stat.rx_packets, stat.rx_bytes, stat.rx_errors,
                             stat.tx_packets, stat.tx_bytes, stat.tx_errors)
