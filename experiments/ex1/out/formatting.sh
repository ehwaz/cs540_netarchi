#!/bin/bash

delstr1="out-of-order"
delstr2="connected"

sed '1,7d' $1 | sed /$delstr1/d | sed /$delstr2/d | sed 's/- /-/' | awk -F"[-]" '{print $2}' | awk '{if($4=="KBytes"){print $1, $3*0.001;}else{print $1, $3;}}' | sed '$d' > $2
