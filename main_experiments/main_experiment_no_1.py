#!/usr/bin/python

"""

CS540 Term project experiment

* I converted this script to use medium-level API
  (no changes in actual function)
* It can be run with external Openflow controller.
  The way to run this script with external controller (on Sangwook's desktop):
  0. set 'runWithExternalController' to 'True'
  1. Run Ryu Openflow controller in same machine
     ryu-manager ~/ryu/ryu/app/simple_switch.py
  2. Run this script
     sudo python ~/cs540_netarchi/mobility.py
* Currently this script does not rely on Openflow controller,
  so this script works whatever controller runs.
  (Currently script just requires connection to the controller) 
  However, this is just an quick-and-dirty version.. There are many things to improve.

* Topology
  Take a look at Lucidchart diagram in google drive directory.
  (Not implmeneted yet)

TODOs
  Implement handover process using Openflow controller. ( )
  Design experiment topology        (v)
  Implement the topology in this file.  ( )

"""

from mininet.net import Mininet
from mininet.node import OVSSwitch
from mininet.util import quietRun
from mininet.log import output, warn

from random import randint
from re import findall

from mininet.cli import CLI
from mininet.node import Controller, Node
from mininet.node import RemoteController
import mininet_example_nat

from mininet.link import Link, Intf
import mininet.ns3
from mininet.ns3 import WIFISegment, WIFIApStaLink

import ns.netanim
import time


def printConnections( switches ):
    "Compactly print connected nodes to each switch"
    for sw in switches:
        output( '%s: ' % sw )
        for intf in sw.intfList():
            link = intf.link
            if link:
                intf1, intf2 = link.intf1, link.intf2
                remote = intf1 if intf1.node != sw else intf2
                output( '%s(%s) ' % ( remote.node, sw.ports[ intf ] ) )
        output( '\n' )


def mobilityTest():
    "A simple test of mobility"
    #runWithExternalController = False
    runWithExternalController = True

    print '* Simple mobility test'
    if runWithExternalController is True:
        print '* Adding remote controller'
        net = Mininet( autoSetMacs=True, autoStaticArp=True)
        c1 = net.addController( 'c1-ryu', controller=RemoteController, ip='127.0.0.1', port=6633)
    else:
        net = Mininet( controller=Controller, autoStaticArp=True)
        c1 = net.addController( 'c1-default', port=6633)

    # Give link option between swtiches and h7
    linkopt = dict(bw=1000, delay='5ms')

    # In our experiment, h6 and h7 is considered as one moving host.
    h5 = net.addHost( 'h5', ip = '10.0.0.5' )
    h6 = net.addHost( 'h6', ip = '10.0.0.6' )

    # h7 runs server.
    h7 = net.addHost( 'h7', ip = '10.0.0.7' )

    print '* Adding links between h# and s#'
    net.addLink( s4, h7, **linkopt)

    WIFIApStaLink(s3, h5)
    WIFIApStaLink(s2, h6)

    host_list = [h5, h6, h7]
    for host in host_list:
        color = (255, 0, 0)
        mininet.ns3.setMobilityModel (host, None)
        ns.netanim.AnimationInterface.SetNodeDescription (host.nsNode, host.name)
        ns.netanim.AnimationInterface.SetNodeColor (host.nsNode, color[0], color[1], color[2])

    switch_list = [s1, s2, s3, s4]
    for switch in switch_list:
        color = (0, 0, 255)
        mininet.ns3.setMobilityModel (switch, None)
        ns.netanim.AnimationInterface.SetNodeDescription (switch.nsNode, switch.name)
        ns.netanim.AnimationInterface.SetNodeColor (switch.nsNode, color[0], color[1], color[2])

    mininet.ns3.setPosition(h7, 20.0, 20.0, 0.0)

    mininet.ns3.setPosition(h5, 80.0, 20.0, 0.0)
    #mininet.ns3.setVelocity(h5, 1.0, 0.0, 0.0)
    mininet.ns3.setPosition(h6, 80.0, 20.0, 0.0)
    #mininet.ns3.setVelocity(h6, 1.0, 0.0, 0.0)

    mininet.ns3.setPosition(s1, 60.0, 150.0, 0.0)

    mininet.ns3.setPosition(s4, 20.0, 100.0, 0.0)
    mininet.ns3.setPosition(s3, 60.0, 100.0, 0.0)
    mininet.ns3.setPosition(s2, 120.0, 100.0, 0.0)

    print '* Adding links between s1 and others. (tree-like)'
    net.addLink( s1, s2, **linkopt )
    net.addLink( s1, s3, **linkopt )
    net.addLink( s1, s4, **linkopt )

    ns.wifi.YansWifiPhyHelper().Default().EnablePcapAll("./pcap-log/wifi")
    anim = ns.netanim.AnimationInterface("./main_experiment.xml")
    anim.EnablePacketMetadata (True)

    print '* Starting network:'
    net.build()
    c1.start()
    s1.start( [ c1 ] )
    s2.start( [ c1 ] )
    s3.start( [ c1 ] )
    s4.start( [ c1 ] )

    net.start()
    mininet.ns3.start()

     print '* Testing network'
    printConnections( net.switches )

    # except h6: moving experiment is from (h5-s3) to (h6-s2).
    host_to_ping = [h5, h7]
    net.pingFull(hosts=host_to_ping)

    #rootNode = mininet_example_nat.connectToInternet( net )

    CLI(net)

    #mininet_example_nat.stopNAT( rootNode )
    mininet.ns3.stop()
    mininet.ns3.clear()
    net.stop()


if __name__ == '__main__':
    mobilityTest()
