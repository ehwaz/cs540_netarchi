#!/usr/bin/python

"""

CS540 Term project experiment

* I converted this script to use medium-level API
  (no changes in actual function)
* It can be run with external Openflow controller.
  The way to run this script with external controller (on Sangwook's desktop):
  0. set 'runWithExternalController' to 'True'
  1. Run Ryu Openflow controller in same machine
     ryu-manager ~/ryu/ryu/app/simple_switch.py
  2. Run this script
     sudo python ~/cs540_netarchi/mobility.py
* Currently this script does not rely on Openflow controller,
  so this script works whatever controller runs.
  (Currently script just requires connection to the controller) 
  However, this is just an quick-and-dirty version.. There are many things to improve.

* Topology
  Take a look at Lucidchart diagram in google drive directory.
  (Not implmeneted yet)

TODOs
  Implement handover process using Openflow controller. ( )
  Design experiment topology        (v)
  Implement the topology in this file.  ( )

"""

from mininet.net import Mininet
from mininet.node import OVSSwitch
from mininet.util import quietRun
from mininet.log import output, warn

from random import randint
from re import findall

from mininet.cli import CLI
from mininet.node import Controller, Node
from mininet.node import RemoteController
import mininet_example_nat

from mininet.link import Link, Intf
import mininet.ns3
from mininet.ns3 import WIFISegment, WIFIApStaLink

class MobilitySwitch( OVSSwitch ):
    "Switch that can reattach and rename interfaces"

    def delIntf( self, intf ):
        "Remove (and detach) an interface"
        port = self.ports[ intf ]
        del self.ports[ intf ]
        del self.intfs[ port ]
        del self.nameToIntf[ intf.name ]

    def addIntf( self, intf, rename=False, **kwargs ):
        "Add (and reparent) an interface"
        OVSSwitch.addIntf( self, intf, **kwargs )
        intf.node = self
        if rename:
            self.renameIntf( intf )

    def attach( self, intf ):
        "Attach an interface and set its port"
        port = self.ports[ intf ]
        if port:
        # Sangwook: in current version of OpenvSwitch,
        # we cannot specify port number when adding port.
        # (this function is supported by 'ofport_request' option in v1.10.0)
            self.cmd( 'ovs-vsctl add-port', self, intf )
        # So, we replace port value in mininet class with port number set by ovs-vsctl.
            ofport = int( self.cmd( 'ovs-vsctl get Interface', intf, 'ofport' ) )
            self.ports[ intf ] = ofport

            #if self.isOldOVS():
            #    self.cmd( 'ovs-vsctl add-port', self, intf )
            #else:
            #    self.cmd( 'ovs-vsctl add-port', self, intf,
            #              '-- set Interface', intf,
            #              'ofport_request=%s' % port )
            #self.validatePort( intf )

    def validatePort( self, intf ):
        "Validate intf's OF port number"
        ofport = int( self.cmd( 'ovs-vsctl get Interface', intf, 'ofport' ) )
        intfport = self.ports[ intf ]
        if ofport != intfport:
            warn( 'WARNING: ofport for', intf, 'is actually', ofport, 
          'but it is set to', intfport,
                  '\n' )

    def renameIntf( self, intf, newname='' ):
        "Rename an interface (to its canonical name)"
        intf.ifconfig( 'down' )
        if not newname:
            newname = '%s-eth%d' % ( self.name, self.ports[ intf ] )
        intf.cmd( 'ip link set', intf, 'name', newname )
        del self.nameToIntf[ intf.name ]
        intf.name = newname
        self.nameToIntf[ intf.name ] = intf
        intf.ifconfig( 'up' )

    def moveIntf( self, intf, switch, rename=True ):
        "Move one of our interfaces to another switch"
        self.detach( intf )
        self.delIntf( intf )
        switch.addIntf( intf, rename=rename )
        switch.attach( intf )


def printConnections( switches ):
    "Compactly print connected nodes to each switch"
    for sw in switches:
        output( '%s: ' % sw )
        for intf in sw.intfList():
            link = intf.link
            if link:
                intf1, intf2 = link.intf1, link.intf2
                remote = intf1 if intf1.node != sw else intf2
                output( '%s(%s) ' % ( remote.node, sw.ports[ intf ] ) )
        output( '\n' )


def moveHost( host, oldSwitch, newSwitch):
    "Move a host from old switch to new switch"
    hintf, sintf = host.connectionsTo( oldSwitch )[ 0 ]
    oldSwitch.moveIntf( sintf, newSwitch)
    return hintf, sintf


def mobilityTest():
    "A simple test of mobility"
    #runWithExternalController = False
    runWithExternalController = True

    print '* Simple mobility test'
    if runWithExternalController is True:
        print '* Adding remote controller'
        net = Mininet( switch=MobilitySwitch )
        c1 = net.addController( 'c1-ryu', controller=RemoteController, ip='127.0.0.1', port=6633)
    else:
        net = Mininet( switch=MobilitySwitch, controller=Controller)
        c1 = net.addController( 'c1-default', port=6633)

    print '* Adding 3 hosts: h1, h2 and h3'
    h1 = net.addHost( 'h1', ip = '10.0.0.1' )
    h2 = net.addHost( 'h2', ip = '10.0.0.2' )
    h3 = net.addHost( 'h3', ip = '10.0.0.3' )

    print '* Adding 3 switches, s1, s2 and s3.'
    s1 = net.addSwitch( 's1', dpid = "11" )
    s2 = net.addSwitch( 's2', dpid = "12" )
    s3 = net.addSwitch( 's3', dpid = "13" )
    #s1 = net.addSwitch( 's1')
    #s2 = net.addSwitch( 's2')
    #s3 = net.addSwitch( 's3')

    print '* Adding links between h# and s#'
    WIFIApStaLink(s1, h1)
    WIFIApStaLink(s2, h2)
    WIFIApStaLink(s3, h3)
    
    # h1 is connected all APs (s1, s2, s3). (Simulating wireless network)
    # In the connection like this, 'mobility' should be controlled in switch's forwarding rule, not by the connection API of mininet. 
    WIFIApStaLink(s2, h1)
    WIFIApStaLink(s3, h1)

    '''
    net.addLink( s1, h1 )
    net.addLink( s2, h2 )
    net.addLink( s3, h3 )
    '''

    print '* Adding links between sN and s(N+1)'
    net.addLink( s1, s2 )
    net.addLink( s2, s3 )

    print '* Starting network:'
    net.build()
    c1.start()
    s1.start( [ c1 ] )
    s2.start( [ c1 ] )
    s3.start( [ c1 ] )
    mininet.ns3.start()

    print '* Testing network'
    printConnections( net.switches )
    net.pingAll()

    #print '* Testing bandwidth between h1 and h2'
    net.iperf( (h1, h2) )

    #print '* Testing bandwidth between h1 and h3'
    net.iperf( (h1, h3) )

    # Sangwook:
    # Changing switch with external controller is not implemented yet.
    '''
    if runWithExternalController is False:
        print '* Identifying switch interface for h1'
        h1, old = net.get( 'h1', 's1' )
        for s in 2, 3, 1:
            new = net.get('s%d' % s)
            print '* Moving', h1, 'from', old, 'to', new
            hintf, sintf = moveHost( h1, old, new)
            print '*', hintf, 'is now connected to', sintf
        print '* Clearing out old flows'
        for sw in net.switches:
            sw.dpctl( 'del-flows' )
        print '* New network:'
        printConnections( net.switches )
        print '* Testing connectivity:'
        net.pingAll()
        old = new
    '''

    rootNode = mininet_example_nat.connectToInternet( net )

    CLI(net)

    mininet_example_nat.stopNAT( rootNode )
    mininet.ns3.stop()
    mininet.ns3.clear()
    net.stop()


if __name__ == '__main__':
    mobilityTest()
